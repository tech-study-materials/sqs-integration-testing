package sandbox.sqs;

import com.tngtech.archunit.core.importer.ImportOption.DoNotIncludeArchives;
import com.tngtech.archunit.core.importer.ImportOption.DoNotIncludeTests;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

@AnalyzeClasses(importOptions = {DoNotIncludeTests.class, DoNotIncludeArchives.class})
public class ArchUnitTests {

    @ArchTest
    public static final ArchRule domainMustNotDependOnAwsSdk =
            noClasses()
                    .that().resideInAPackage("sandbox.sqs.domain..")
                    .should().dependOnClassesThat().resideInAPackage("com.amazonaws..");

    @ArchTest
    public static final ArchRule restMustNotDependOnAwsSdk =
            noClasses()
                    .that().resideInAPackage("sandbox.sqs.adapters.rest..")
                    .should().dependOnClassesThat().resideInAPackage("com.amazonaws..");

}
