package sandbox.sqs;

import com.amazonaws.services.sqs.AmazonSQSAsync;
import com.amazonaws.services.sqs.AmazonSQSAsyncClientBuilder;
import lombok.Getter;
import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.testcontainers.containers.localstack.LocalStackContainer;
import org.testcontainers.utility.DockerImageName;

import static org.testcontainers.containers.localstack.LocalStackContainer.Service.SQS;
import static org.testcontainers.utility.DockerImageName.parse;

public class LocalStackSupport implements BeforeAllCallback, AfterAllCallback {

    private final LocalStackContainer container = new LocalStackContainer(parse("localstack/localstack:0.12.3"))
            .withServices(SQS)
            .withEnv("DEFAULT_REGION", "eu-central-1");

    @Getter AmazonSQSAsync sqs;

    @Override
    public void beforeAll(ExtensionContext extensionContext) {
        container.start();
        sqs = AmazonSQSAsyncClientBuilder.standard()
            .withCredentials(container.getDefaultCredentialsProvider())
            .withEndpointConfiguration(container.getEndpointConfiguration(SQS))
            .build();
        createTestQueues();
    }

    @Override
    public void afterAll(ExtensionContext context) {
        container.stop();
    }

    void createTestQueues() {
        sqs.createQueue("inbound-test-queue");
        sqs.createQueue("outbound-test-queue");
    }

    public String urlOf(String queueName) {
        return sqs.getQueueUrl(queueName).getQueueUrl();
    }
}
