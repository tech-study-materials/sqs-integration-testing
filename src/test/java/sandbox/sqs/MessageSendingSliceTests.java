package sandbox.sqs;

import com.amazonaws.services.sqs.AmazonSQSAsync;
import com.amazonaws.services.sqs.model.Message;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.testcontainers.containers.localstack.LocalStackContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import sandbox.sqs.adapters.sqs.SqsConfig;
import sandbox.sqs.adapters.sqs.SqsReceiver;
import sandbox.sqs.adapters.sqs.SqsSender;
import sandbox.sqs.domain.AppMessage;
import sandbox.sqs.domain.SomeBusinessServiceConsumingMessages;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.NONE;

@SpringBootTest(webEnvironment = NONE, classes = {
		SqsConfig.class,
		SqsSender.class,
		MessageSendingSliceTests.Config.class,
		JacksonAutoConfiguration.class})
class MessageSendingSliceTests {

	@RegisterExtension static LocalStackSupport localStack = new LocalStackSupport();

	@Autowired ObjectMapper jackson;
	@Autowired SqsSender sender;

	@Test
	void shouldBeAbleToReceiveMessage() {
		AppMessage sentByApp = new AppMessage("test message @ " + System.currentTimeMillis());
		sender.send(sentByApp);

		var receivedMessages = localStack.getSqs()
				.receiveMessage(localStack.urlOf("outbound-test-queue"))
				.getMessages()
				.stream()
				.map(Message::getBody)
				.peek(System.out::println)
				.map(this::parseAppMessage);

		assertThat(receivedMessages).containsExactly(sentByApp);
	}

	@SneakyThrows
	private AppMessage parseAppMessage(String json) {
		return jackson.readValue(json, AppMessage.class);
	}

	@Configuration
	static class Config {
		@Bean
		public AmazonSQSAsync amazonSQS() {
			return localStack.getSqs();
		}
	}
}
