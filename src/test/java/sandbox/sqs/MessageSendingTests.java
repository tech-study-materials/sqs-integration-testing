package sandbox.sqs;

import com.amazonaws.services.sqs.AmazonSQSAsync;
import com.amazonaws.services.sqs.model.Message;
import sandbox.sqs.domain.AppMessage;
import sandbox.sqs.domain.SomeBusinessServiceProducingMessages;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.testcontainers.containers.localstack.LocalStackContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class MessageSendingTests {

	@RegisterExtension static LocalStackSupport localStack = new LocalStackSupport();

	@Autowired SomeBusinessServiceProducingMessages producer;
	@Autowired ObjectMapper jackson;

	@Test
	void shouldBeAbleToSendMessage() {
		var messageText = "test message @ " + System.currentTimeMillis();
		producer.sendMessage(messageText);

		var receivedMessages = localStack.getSqs()
				.receiveMessage(localStack.urlOf("outbound-test-queue"))
				.getMessages()
				.stream()
				.map(Message::getBody)
				.peek(System.out::println)
				.map(this::parseAppMessage);

		assertThat(receivedMessages).containsExactly(new AppMessage(messageText));
	}

	@SneakyThrows
	private AppMessage parseAppMessage(String json) {
		return jackson.readValue(json, AppMessage.class);
	}

	@TestConfiguration
	static class Config {
		@Bean
		public AmazonSQSAsync amazonSQS() {
			return localStack.getSqs();
		}
	}
}
