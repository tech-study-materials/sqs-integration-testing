package sandbox.sqs;

import com.amazonaws.services.sqs.AmazonSQSAsync;
import sandbox.sqs.domain.AppMessage;
import sandbox.sqs.domain.SomeBusinessServiceConsumingMessages;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.testcontainers.containers.localstack.LocalStackContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;

@SpringBootTest
class MessageReceivingTests {

	@RegisterExtension static LocalStackSupport localStack = new LocalStackSupport();

	@Autowired ObjectMapper jackson;
	@Autowired SomeBusinessServiceConsumingMessages consumer;

	@Test
	void shouldBeAbleToReceiveMessage() throws JsonProcessingException {
		AppMessage toBeSent = new AppMessage("test message @ " + System.currentTimeMillis());
		localStack.getSqs().sendMessage(
				localStack.urlOf("inbound-test-queue"),
				jackson.writeValueAsString(toBeSent));

		await().untilAsserted(() -> assertThat(consumer.getLastReceived()).isEqualTo(toBeSent));

	}

	@TestConfiguration
	static class Config {
		@Bean
		public AmazonSQSAsync amazonSQS() {
			return localStack.getSqs();
		}
	}
}
