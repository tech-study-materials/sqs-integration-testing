package sandbox.sqs;

import com.amazonaws.services.sqs.AmazonSQSAsync;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJson;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.aws.autoconfigure.messaging.MessagingAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.testcontainers.containers.localstack.LocalStackContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import sandbox.sqs.adapters.sqs.SqsConfig;
import sandbox.sqs.adapters.sqs.SqsReceiver;
import sandbox.sqs.domain.AppMessage;
import sandbox.sqs.domain.SomeBusinessServiceConsumingMessages;

import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;
import static org.mockito.Mockito.verify;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.NONE;

@SpringBootTest(webEnvironment = NONE, classes = {
		SqsConfig.class,
		SqsReceiver.class,
		MessageReceivingSliceTests.Config.class,
		MessagingAutoConfiguration.SqsAutoConfiguration.class, //creates SimpleMessageListenerContainer, so that @SqsListeners are properly registered
		JacksonAutoConfiguration.class})
class MessageReceivingSliceTests {

	@RegisterExtension static LocalStackSupport localStack = new LocalStackSupport();

	@Autowired ObjectMapper jackson;
	@MockBean SomeBusinessServiceConsumingMessages consumer;

	@Test
	void shouldBeAbleToReceiveMessage() throws JsonProcessingException {
		AppMessage toBeSent = new AppMessage("test message @ " + System.currentTimeMillis());
		localStack.getSqs().sendMessage(
				localStack.urlOf("inbound-test-queue"),
				jackson.writeValueAsString(toBeSent));

		await().untilAsserted(() -> verify(consumer).setLastReceived(toBeSent));
	}

	@Configuration
	static class Config {
		@Bean
		public AmazonSQSAsync amazonSQS() {
			return localStack.getSqs();
		}
	}
}
