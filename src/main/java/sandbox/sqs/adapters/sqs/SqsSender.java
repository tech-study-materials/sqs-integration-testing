package sandbox.sqs.adapters.sqs;

import sandbox.sqs.domain.AppMessage;
import sandbox.sqs.domain.MessageSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate;
import org.springframework.stereotype.Component;

@Component
public class SqsSender implements MessageSender {

    @Autowired QueueMessagingTemplate template;
    @Value("${queues.outbound}") String outboundQueueName;

    @Override
    public void send(AppMessage message) {
        template.convertAndSend(outboundQueueName, message);
    }
}
