package sandbox.sqs.adapters.sqs;

import sandbox.sqs.domain.AppMessage;
import sandbox.sqs.domain.SomeBusinessServiceConsumingMessages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.aws.messaging.listener.annotation.SqsListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class SqsReceiver {

    @Autowired
    SomeBusinessServiceConsumingMessages someBusinessServiceConsumingMessages;

    @SqsListener("${queues.inbound}")
    public void receiveMessage(@Payload AppMessage message) {
        someBusinessServiceConsumingMessages.setLastReceived(message);
    }
}
