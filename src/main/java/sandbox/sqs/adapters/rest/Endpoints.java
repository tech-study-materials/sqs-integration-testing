package sandbox.sqs.adapters.rest;

import sandbox.sqs.domain.AppMessage;
import sandbox.sqs.domain.SomeBusinessServiceConsumingMessages;
import sandbox.sqs.domain.SomeBusinessServiceProducingMessages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Endpoints {

    @Autowired SomeBusinessServiceConsumingMessages consumer;
    @Autowired SomeBusinessServiceProducingMessages producer;

    @GetMapping("/last-received")
    AppMessage lastReceived() {
        return consumer.getLastReceived();
    }

    @GetMapping("/send/{text}")
    String send(@PathVariable String text) {
        producer.sendMessage(text);
        return "ok";
    }

    @GetMapping("/")
    String health() {
        return "Status: UP";
    }
}
