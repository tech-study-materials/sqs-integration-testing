package sandbox.sqs.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;

@Service
public class SomeBusinessServiceConsumingMessages {

    @Getter @Setter
    AppMessage lastReceived = new AppMessage("did not receive anything yet");
}
