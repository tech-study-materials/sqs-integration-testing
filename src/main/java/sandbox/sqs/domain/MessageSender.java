package sandbox.sqs.domain;

public interface MessageSender {
    void send(AppMessage message);
}
