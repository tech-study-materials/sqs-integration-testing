package sandbox.sqs.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SomeBusinessServiceProducingMessages {

    @Autowired MessageSender messageSender;

    public void sendMessage(String text) {
        messageSender.send(new AppMessage(text));
    }
}
